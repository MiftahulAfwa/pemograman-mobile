class Mahasiswa {
  int Id;
  String Nama;

  Mahasiswa({required this.Id, required this.Nama});

  factory Mahasiswa.fromJson(Map<String, dynamic> json) {
    return Mahasiswa(
      Id: json['Id'],
      Nama: json['Nama'],
    );
  }

  Map<String, dynamic> toJson() => {
        'Id': Id,
        'Nama': Nama,
      };
}
