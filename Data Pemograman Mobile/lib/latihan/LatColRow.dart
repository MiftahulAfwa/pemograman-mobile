import 'package:flutter/material.dart';

class LatColRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App ke Dua',
      home: Scaffold(
        backgroundColor: Colors.yellowAccent,
        appBar: AppBar(
          backwardsCompatibility: false,
          title: Text('Welcome diFlutter'),
          backgroundColor: Colors.deepOrange,
          foregroundColor: Colors.blueGrey,
          shadowColor: Colors.black,
        ),
        body: Column(children: <Widget>[
          Image.asset('assets/images/pano1.png'),
          Text(
            'pembelalajaran Flutter Untuk Pemula',
            style: TextStyle(fontSize: 33, fontFamily: "Serif", height: 2.0),
          ),
          Text('Universitas Pradita'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset('assets/images/strawberry.png'),
              Image.asset('assets/images/flamingo.png')
            ],
          )
        ]),
      ),
    );
  }
}
