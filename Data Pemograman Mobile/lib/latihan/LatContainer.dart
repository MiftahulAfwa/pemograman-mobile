import 'package:flutter/material.dart';

class LatContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App ke Tiga',
      home: Scaffold(
        backgroundColor: Colors.yellowAccent,
        appBar: AppBar(
          backwardsCompatibility: false,
          title: Text('Selamat datang diFlutter'),
          backgroundColor: Colors.blue,
          foregroundColor: Colors.redAccent,
          shadowColor: Colors.black,
        ),
        body: Column(children: <Widget>[
          Image.asset('assets/images/pano32.png'),
          Text(
            'pembelalajaran Flutter Untuk Pemula',
            style: TextStyle(fontSize: 35, fontFamily: "Serif", height: 2.0),
          ),
          Text('Universitas Pradita'),
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 10, color: Colors.black38),
                borderRadius: const BorderRadius.all(Radius.circular(8)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Image.asset('assets/images/strawberry.png'),
                  Image.asset('assets/images/flamingo.png')
                ],
              ))
        ]),
      ),
    );
  }
}
