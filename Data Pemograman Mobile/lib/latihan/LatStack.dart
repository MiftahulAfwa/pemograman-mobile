import 'package:flutter/material.dart';

class LatStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Applikasi ke Lima',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome diFlutter'),
        ),
        body: Stack(
          children: [
            const CircleAvatar(
              backgroundImage: AssetImage('assets/images/laut.png'),
              radius: 100,
            ),
            Container(
              width: 200,
              height: 200,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: Colors.black12,
              ),
              child: const Text(
                'CJ',
                style: TextStyle(
                  fontSize: 29,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
