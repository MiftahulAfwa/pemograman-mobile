import 'package:flutter/material.dart';

class LatListView extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.pink,
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Center(
            child: Text(
              'Flutter Demo',
              style: TextStyle(
                color: Colors.orange,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
          ),
        ),
        body: ListView(
          children: List.generate(
            30,
            (index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 10, color: Colors.black38),
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                ),
                child: Text('View $index'),
              );
            },
          ),
        ),
      ),
    );
  }
}
